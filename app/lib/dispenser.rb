# frozen_string_literal: true

class Dispenser

  ##
  # Dispenses the specified amount of bottles
  # @param count [Integer] The number of bottles
  # @example:
  #  Dispenser.dispense_kolas( 7 )
  def self.dispense_kolas( count )
    true
  end

  ##
  # Dispenses the specified coins
  # @param coins [Array<Integer>] Coins to dispense
  # @example:
  #  Dispenser.dispense_coins([ 100, 50, 20, 1, 1, 1 ]) # to dispence €1.73
  def self.dispense_coins( coins )
    true
  end
end
