The badly designed Fritz-Kola Vending Machine
=============================================

![Fritz-Kola](app/assets/images/Fritz-Kola.jpg "Fritz-Kola")


Several silly developers decided to design a vending machine with a web interface written in Ruby on Rails. The machine sells __Fritz-Kolas__ for `€2.17`.

The machine is also stocked up in coins with the following denominations: `1c, 5c, 10c, 20c, 50c and €2`.

The machine accepts a single banknote (`€5, €10, €20, €50, €100, €200, €500`) and returns the maximum amount of Fritz Kolas the user can buy plus any change that is left, such that it uses the **minimum** amount of coins.

You are the back end developer. All you need to write is the code that processes requests and calculates how many kolas to dispense, and what change to return. The Front End will send you a single parameter `banknote` containing the amount of the banknote in `EUR`.

To controll the dispenser use the `Dispenser` class. Documentation in the `app/lib/dispenser.rb` file.

Write the controller code and specs.

_**Note:** You can use the Rails app in this project. It's based on Rails `6.0.2` running on Ruby `2.7.1`. If you don't have these pre-installed on your computer you can create a new Rails app yourself as installing these specific versions can take a lot of time._

Good Luck!
